package com.histler.voltmobi;

import android.app.Application;

/**
 * Created by Badr
 * on 28.05.2016 23:19.
 */
public class VoltmobiApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Initializer.initialize();
    }
}
