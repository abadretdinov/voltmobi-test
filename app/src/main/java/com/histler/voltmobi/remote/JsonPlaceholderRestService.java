package com.histler.voltmobi.remote;

import com.histler.voltmobi.entitiy.Post;

import retrofit.http.GET;

/**
 * Created by Badr
 * on 25.05.2016 14:09
 */
public interface JsonPlaceholderRestService {
    @GET("/posts")
    Post.List getPosts();
}
