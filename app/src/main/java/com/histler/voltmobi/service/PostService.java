package com.histler.voltmobi.service;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.histler.base.dao.DatabaseManager;
import com.histler.voltmobi.AppBeanContainer;
import com.histler.voltmobi.dao.FavPostDao;
import com.histler.voltmobi.dao.PostDao;
import com.histler.voltmobi.entitiy.FavPost;
import com.histler.voltmobi.entitiy.Post;

import java.util.List;

/**
 * Created by Badr
 * on 29.05.2016 1:34.
 */
public class PostService {
    public Post.List getPosts(Context context) {
        AppBeanContainer appBeanContainer = AppBeanContainer.INSTANCE;
        DatabaseManager manager = DatabaseManager.getInstance(context);
        PostDao postDao = appBeanContainer.getPostDao();
        SQLiteDatabase database = manager.openWritableDatabase();
        try {
            return new Post.List(postDao.getAllEntities(database));
        } finally {
            manager.closeDatabase();
        }
    }

    public Post getPost(Context context, long id) {
        AppBeanContainer appBeanContainer = AppBeanContainer.INSTANCE;
        DatabaseManager manager = DatabaseManager.getInstance(context);
        PostDao postDao = appBeanContainer.getPostDao();
        SQLiteDatabase database = manager.openWritableDatabase();
        try {
            return postDao.getById(database, id);
        } finally {
            manager.closeDatabase();
        }
    }

    public FavPost getFavPost(Context context, long id) {
        AppBeanContainer appBeanContainer = AppBeanContainer.INSTANCE;
        DatabaseManager manager = DatabaseManager.getInstance(context);
        FavPostDao favPostDao = appBeanContainer.getFavPostDao();
        PostDao postDao = appBeanContainer.getPostDao();
        SQLiteDatabase database = manager.openWritableDatabase();
        try {
            Post post = postDao.getById(database, id);
            return new FavPost(post, favPostDao.isFav(database, id));
        } finally {
            manager.closeDatabase();
        }
    }

    public FavPost.List getFavPosts(Context context) {
        AppBeanContainer appBeanContainer = AppBeanContainer.INSTANCE;
        DatabaseManager manager = DatabaseManager.getInstance(context);
        FavPostDao favPostDao = appBeanContainer.getFavPostDao();
        PostDao postDao = appBeanContainer.getPostDao();
        SQLiteDatabase database = manager.openWritableDatabase();
        try {
            FavPost.List list = new FavPost.List();
            List<Post> posts = postDao.getAllEntities(database);
            for (Post post : posts) {
                list.add(new FavPost(post, favPostDao.isFav(database, post.getId())));
            }
            return list;
        } finally {
            manager.closeDatabase();
        }
    }

    public FavPost.List getOnlyFavPosts(Context context) {
        AppBeanContainer appBeanContainer = AppBeanContainer.INSTANCE;
        DatabaseManager manager = DatabaseManager.getInstance(context);
        FavPostDao favPostDao = appBeanContainer.getFavPostDao();
        PostDao postDao = appBeanContainer.getPostDao();
        SQLiteDatabase database = manager.openWritableDatabase();
        try {
            List<Long> favIds = favPostDao.getAllFav(database);
            FavPost.List list = new FavPost.List();
            for (Long favId : favIds) {
                Post post = postDao.getById(database, favId);
                list.add(new FavPost(post, true));
            }
            return list;
        } finally {
            manager.closeDatabase();
        }
    }

    public boolean isFav(Context context, long id) {
        AppBeanContainer appBeanContainer = AppBeanContainer.INSTANCE;
        DatabaseManager manager = DatabaseManager.getInstance(context);
        FavPostDao favPostDao = appBeanContainer.getFavPostDao();
        SQLiteDatabase database = manager.openWritableDatabase();
        try {
            return favPostDao.isFav(database, id);
        } finally {
            manager.closeDatabase();
        }
    }

    public void setFav(Context context, FavPost favPost) {
        AppBeanContainer appBeanContainer = AppBeanContainer.INSTANCE;
        DatabaseManager manager = DatabaseManager.getInstance(context);
        FavPostDao favPostDao = appBeanContainer.getFavPostDao();
        SQLiteDatabase database = manager.openWritableDatabase();
        try {
            if (favPost.isFav()) {
                favPostDao.addFav(database, favPost.getId());
            } else {
                favPostDao.deleteFav(database, favPost.getId());
            }
        } finally {
            manager.closeDatabase();
        }
    }

    public void savePosts(Context context, Post.List posts) {
        AppBeanContainer appBeanContainer = AppBeanContainer.INSTANCE;
        DatabaseManager manager = DatabaseManager.getInstance(context);
        PostDao postDao = appBeanContainer.getPostDao();
        SQLiteDatabase database = manager.openWritableDatabase();
        try {
            postDao.deleteAll(database);
            postDao.saveAll(database, posts);
        } finally {
            manager.closeDatabase();
        }
    }
}
