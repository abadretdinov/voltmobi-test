package com.histler.voltmobi.service;

import android.support.v4.app.Fragment;

import com.histler.base.activity.BaseActivity;
import com.histler.base.service.NavigationService;
import com.histler.voltmobi.activity.FragmentWrapperActivity;
import com.histler.voltmobi.activity.MainActivity;
import com.histler.voltmobi.fragment.PostsFragment;

/**
 * Created by Badr
 * on 28.05.2016.
 */
public class NavigationServiceImpl implements NavigationService {
    @Override
    public Class<? extends BaseActivity> getMainActivityClass() {
        return MainActivity.class;
    }

    @Override
    public Class<? extends BaseActivity> getActivityClass() {
        return FragmentWrapperActivity.class;
    }

    @Override
    public Class<? extends Fragment> getDefaultFragment() {
        return PostsFragment.class;
    }
}
