package com.histler.voltmobi.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.histler.base.adapter.BaseRecyclerAdapter;
import com.histler.voltmobi.R;
import com.histler.voltmobi.adapter.viewholder.FavPostViewHolder;
import com.histler.voltmobi.entitiy.FavPost;

/**
 * Created by Badr on 28.05.2016.
 */
public class FavPostsAdapter extends BaseRecyclerAdapter<FavPost, FavPostViewHolder> {
    private FavPostViewHolder.OnFavClickListener mOnFavClickListener;

    public FavPostsAdapter(FavPost.List data, FavPostViewHolder.OnFavClickListener onFavClickListener) {
        super(data);
        mOnFavClickListener = onFavClickListener;
    }

    public void changeItem(FavPost item) {
        int position = mData.indexOf(item);
        if (position >= 0) {
            mData.set(position, item);
            notifyItemChanged(position);
        }

    }

    @Override
    public FavPostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.fav_post_row, parent, false);
        return new FavPostViewHolder(view, mOnItemClickListener, mOnFavClickListener);
    }

    @Override
    public void onBindViewHolder(FavPostViewHolder holder, int position) {
        FavPost favPost = getItem(position);
        holder.setPost(favPost);
    }
}
