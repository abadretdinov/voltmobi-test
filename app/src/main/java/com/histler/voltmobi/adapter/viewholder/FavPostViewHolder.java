package com.histler.voltmobi.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.histler.base.adapter.OnItemClickListener;
import com.histler.base.adapter.viewholder.BaseViewHolder;
import com.histler.voltmobi.R;
import com.histler.voltmobi.entitiy.FavPost;

/**
 * Created by Badr
 * on 28.05.2016.
 */
public class FavPostViewHolder extends BaseViewHolder {
    TextView title;
    TextView body;
    ImageView fav;
    private OnFavClickListener mOnFavClickListener;

    public FavPostViewHolder(View itemView, OnItemClickListener clickListener, OnFavClickListener onFavClickListener) {
        super(itemView, clickListener);
        mOnFavClickListener = onFavClickListener;
    }

    @Override
    protected void initView(View itemView) {
        title = (TextView) itemView.findViewById(android.R.id.text1);
        body = (TextView) itemView.findViewById(android.R.id.text2);
        fav = (ImageView) itemView.findViewById(R.id.fav);
        fav.setOnClickListener(this);
    }

    public void setPost(FavPost post) {
        title.setText(post.getTitle());
        body.setText(post.getBody());
        fav.setImageResource(post.isFav() ? R.drawable.ic_star_filled : R.drawable.ic_star);
    }

    @Override
    public void onClick(View view) {
        if (view.equals(fav) && mOnFavClickListener != null) {
            mOnFavClickListener.onFavClicked(itemView, view, getLayoutPosition());
        } else {
            super.onClick(view);
        }

    }

    public interface OnFavClickListener {
        void onFavClicked(View itemView, View favView, int position);
    }
}
