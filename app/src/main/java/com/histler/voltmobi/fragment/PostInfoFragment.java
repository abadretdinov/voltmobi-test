package com.histler.voltmobi.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.histler.base.fragment.BaseFragment;
import com.histler.base.util.DialogUtils;
import com.histler.base.util.Navigate;
import com.histler.base.util.robospice.LocalSpiceService;
import com.histler.voltmobi.R;
import com.histler.voltmobi.entitiy.FavPost;
import com.histler.voltmobi.task.FavPostGetRequest;
import com.histler.voltmobi.task.PostFavChangeRequest;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Badr
 * on 29.05.2016 4:11.
 */
public class PostInfoFragment extends BaseFragment implements RequestListener<FavPost>, View.OnClickListener {
    private final SpiceManager mSpiceManager = new SpiceManager(LocalSpiceService.class);

    private TextView mTitleView;
    private TextView mBodyView;
    private ImageView mFavView;

    private FavPost mFavPost;

    protected SpiceManager getSpiceManager() {
        return mSpiceManager;
    }

    @Override
    public void onStart() {
        if (!getSpiceManager().isStarted()) {
            getSpiceManager().start(getActivity().getApplicationContext());
            loadFavPost();
        }
        super.onStart();
    }

    private void loadFavPost() {
        if (getArguments() != null) {
            if (getArguments().containsKey(Navigate.PARAM_ID)) {
                long id = getArguments().getLong(Navigate.PARAM_ID);
                getSpiceManager().execute(new FavPostGetRequest(getContext(), id), this);
            } else if (getArguments().containsKey(Navigate.PARAM_ENTITY)) {
                mFavPost = (FavPost) getArguments().getSerializable(Navigate.PARAM_ENTITY);
                initView();
            }
        } else {
            moveBack();
        }
    }

    @Override
    public void onDestroy() {
        if (getSpiceManager().isStarted()) {
            getSpiceManager().shouldStop();
        }
        super.onDestroy();
    }

    @Override
    protected String getTitle() {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.post_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTitleView = (TextView) view.findViewById(android.R.id.text1);
        mBodyView = (TextView) view.findViewById(android.R.id.text2);
        mFavView = (ImageView) view.findViewById(R.id.fav);
        mFavView.setOnClickListener(this);
    }

    private void initView() {
        if (mFavPost != null) {
            mTitleView.setText(mFavPost.getTitle());
            mBodyView.setText(mFavPost.getBody());
            initFavBtn();
        }
    }

    private void initFavBtn() {
        mFavView.setImageResource(mFavPost.isFav() ? R.drawable.ic_star_filled : R.drawable.ic_star);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(mFavView)) {
            if (mFavPost.isFav()) {
                DialogUtils.showYesNoDialog(v.getContext(), getString(R.string.delete_from_fav_title), getString(R.string.delete_from_fav_message), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        changeFav();
                        dialog.dismiss();
                    }
                });
            } else {
                changeFav();
            }
        }
    }

    private void changeFav() {
        boolean wasFav = mFavPost.isFav();
        mFavPost.setFav(!wasFav);
        initFavBtn();
        getSpiceManager().execute(new PostFavChangeRequest(getContext(), mFavPost), new PostFavChangeListener(wasFav));
    }

    @Subscribe
    public void onPostFavChanged(FavPost favPost) {
        if (mFavPost != null && mFavPost.getId() == favPost.getId()) {
            mFavPost.setFav(favPost.isFav());
            initFavBtn();
        }
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        if (isAdded()) {
            showMessage(getString(R.string.request_with_error));
            moveBack();
        }
    }

    @Override
    public void onRequestSuccess(FavPost favPost) {
        if (isAdded()) {
            mFavPost = favPost;
            initView();
        }
    }

    private class PostFavChangeListener implements RequestListener<FavPost> {
        private boolean mWasFav;

        public PostFavChangeListener(boolean wasFav) {
            mWasFav = wasFav;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            if (isAdded()) {
                mFavPost.setFav(mWasFav);
                initFavBtn();
                showMessage(getString(R.string.error_adding_to_fav));
            }
        }

        @Override
        public void onRequestSuccess(FavPost favPost) {
            EventBus.getDefault().post(favPost);
        }
    }
}
