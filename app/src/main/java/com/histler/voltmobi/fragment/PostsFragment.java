package com.histler.voltmobi.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.histler.base.fragment.BaseRecyclerFragment;
import com.histler.base.util.DialogUtils;
import com.histler.base.util.Navigate;
import com.histler.base.util.NetworkUtils;
import com.histler.base.util.robospice.LocalSpiceService;
import com.histler.voltmobi.R;
import com.histler.voltmobi.adapter.FavPostsAdapter;
import com.histler.voltmobi.adapter.viewholder.FavPostViewHolder;
import com.histler.voltmobi.entitiy.FavPost;
import com.histler.voltmobi.task.FavPostsLoadRequest;
import com.histler.voltmobi.task.LocalFavPostsGetRequest;
import com.histler.voltmobi.task.PostFavChangeRequest;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Badr
 * on 26.05.2016 17:47
 */
public class PostsFragment extends BaseRecyclerFragment<FavPost, FavPostViewHolder> implements FavPostViewHolder.OnFavClickListener, View.OnClickListener {
    private final SpiceManager mSpiceManager = new SpiceManager(LocalSpiceService.class);
    private final PostsLoadListener mPostsLoadListener = new PostsLoadListener();

    protected SpiceManager getSpiceManager() {
        return mSpiceManager;
    }

    @Override
    public void onStart() {
        if (!getSpiceManager().isStarted()) {
            getSpiceManager().start(getActivity().getApplicationContext());
            onRefresh();
        }
        super.onStart();
    }

    @Override
    public void onDestroy() {
        if (getSpiceManager().isStarted()) {
            getSpiceManager().shouldStop();
        }
        super.onDestroy();
    }

    public void onHardRefresh() {
        setHardRefreshing();
        onRefresh();
    }

    @Override
    public void onRefresh() {
        Context context = getContext();
        if (NetworkUtils.isNetworkAvailable(context)) {
            getSpiceManager().execute(new FavPostsLoadRequest(context), mPostsLoadListener);
        } else {
            getSpiceManager().execute(new LocalFavPostsGetRequest(context), mPostsLoadListener);
        }
    }

    @Override
    protected String getTitle() {
        return getString(R.string.posts);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getEmptyView().findViewById(R.id.empty_btn).setOnClickListener(this);
    }

    @Override
    public void onFavClicked(View itemView, View favView, final int position) {
        FavPostsAdapter adapter = (FavPostsAdapter) getAdapter();
        FavPost favPost = adapter.getItem(position);
        if (favPost.isFav()) {
            DialogUtils.showYesNoDialog(itemView.getContext(), getString(R.string.delete_from_fav_title), getString(R.string.delete_from_fav_message), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    changeFav(position);
                    dialog.dismiss();
                }
            });//todo возможна утечка, если во время создания диалога откроется звонилка. перевести на DialogFragment
        } else {
            changeFav(position);
        }
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        FavPostsAdapter adapter = (FavPostsAdapter) getAdapter();
        FavPost favPost = adapter.getItem(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Navigate.PARAM_ENTITY, favPost);
        Navigate.to(v.getContext(), PostInfoFragment.class, bundle);
    }

    private void changeFav(int position) {
        FavPostsAdapter adapter = (FavPostsAdapter) getAdapter();
        FavPost favPost = adapter.getItem(position);
        boolean wasFav = favPost.isFav();
        //so there's no delay for user
        favPost.setFav(!wasFav);
        adapter.notifyItemChanged(position);
        getSpiceManager().execute(new PostFavChangeRequest(getContext(), favPost), new PostFavChangeListener(position, wasFav));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.empty_btn) {
            onHardRefresh();
        }
    }

    /*we get extra post update on request success, but it worth it, since no delay for user*/
    @Subscribe
    public void onPostFavChanged(FavPost favPost) {
        FavPostsAdapter adapter = (FavPostsAdapter) getAdapter();
        if (adapter != null) {
            adapter.changeItem(favPost);
        }
    }

    private class PostFavChangeListener implements RequestListener<FavPost> {
        private int mPosition;
        private boolean mWasFav;

        public PostFavChangeListener(int position, boolean wasFav) {
            this.mPosition = position;
            mWasFav = wasFav;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            if (isAdded()) {
                FavPostsAdapter adapter = (FavPostsAdapter) getAdapter();
                if (adapter != null) {
                    adapter.getItem(mPosition).setFav(mWasFav);
                    adapter.notifyItemChanged(mPosition);
                }
                showMessage(getString(R.string.error_adding_to_fav));
            }
        }

        @Override
        public void onRequestSuccess(FavPost favPost) {
            EventBus.getDefault().post(favPost);
        }
    }

    private class PostsLoadListener implements RequestListener<FavPost.List> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            if (isAdded()) {
                setRefreshed();
                showMessage(getString(R.string.request_with_error), getString(R.string.repeat), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onHardRefresh();
                    }
                });
                //showMessage(spiceException.getCause() != null ? spiceException.getCause().getMessage() : spiceException.getMessage());
            }
        }

        @Override
        public void onRequestSuccess(FavPost.List posts) {
            if (isAdded()) {
                setRefreshed();
                setAdapter(new FavPostsAdapter(posts, PostsFragment.this));
            }
        }
    }
}
