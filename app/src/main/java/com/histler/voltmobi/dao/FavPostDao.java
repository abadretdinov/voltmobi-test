package com.histler.voltmobi.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.histler.base.dao.CreateTableDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Badr
 * on 29.05.2016 0:24.
 */
public class FavPostDao implements CreateTableDao {
    private static final String TABLE_NAME = "fav_posts";
    private static final String COLUMN_ID = "_id";

    private String getTableName() {
        return TABLE_NAME;
    }

    private String[] getAllColumns() {
        return new String[]{COLUMN_ID};
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL("create table if not exists " + getTableName() + " (" + COLUMN_ID + " integer not null);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

    }

    private ContentValues toContentValues(long id) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, id);
        return values;
    }

    public boolean addFav(SQLiteDatabase database, long id) {
        ContentValues values = toContentValues(id);
        return database.insert(getTableName(), null, values) == id;
    }

    public boolean deleteFav(SQLiteDatabase database, long id) {
        return database.delete(getTableName(), COLUMN_ID + "=?", new String[]{String.valueOf(id)}) > 0;
    }

    public List<Long> getAllFav(SQLiteDatabase database) {
        Cursor cursor = database.query(getTableName(), getAllColumns(), null, null, null, null, null);
        try {
            List<Long> entities = new ArrayList<>(cursor.getCount());
            if (cursor.moveToFirst()) {
                do {
                    Long entity = cursor.getLong(0);
                    entities.add(entity);
                } while (cursor.moveToNext());
            }
            return entities;
        } finally {
            cursor.close();
        }

    }

    public boolean isFav(SQLiteDatabase database, long id) {
        Cursor cursor = database.query(getTableName(), getAllColumns(), COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null, null, null);
        try {
            return cursor.moveToFirst();
        } finally {
            cursor.close();
        }
    }
}
