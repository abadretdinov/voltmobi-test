package com.histler.voltmobi.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.histler.base.dao.GeneralDao;
import com.histler.voltmobi.entitiy.Post;

/**
 * Created by Badr
 * on 28.05.2016 23:57.
 */
public class PostDao extends GeneralDao<Post> {
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_BODY = "body";
    public static final String COLUMN_USER_ID = "user_id";

    private static final String TABLE_NAME = "posts";

    private static final String[] ALL_COLUMNS = {
            COLUMN_ID,
            COLUMN_TITLE,
            COLUMN_BODY,
            COLUMN_USER_ID
    };

    private static final String CREATE_TABLE_QUERY =
            "( " +
                    COLUMN_ID + " integer primary key, " +
                    COLUMN_TITLE + " text default null, " +
                    COLUMN_BODY + " text default null, " +
                    COLUMN_USER_ID + " integer default null " +
                    ");";

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String[] getAllColumns() {
        return ALL_COLUMNS;
    }

    @Override
    public String getNoTableNameCreateTableQuery() {
        return CREATE_TABLE_QUERY;
    }

    @Override
    public Post cursorToEntity(Cursor cursor, int index) {
        Post post = new Post();
        int i = index;
        post.setId(cursor.getLong(i));
        i++;
        post.setTitle(cursor.getString(i));
        i++;
        post.setBody(cursor.getString(i));
        i++;
        post.setUserId(cursor.getLong(i));
        return post;
    }

    @Override
    protected ContentValues entityToContentValues(Post entity) {
        ContentValues values = super.entityToContentValues(entity);
        if (!TextUtils.isEmpty(entity.getTitle())) {
            values.put(COLUMN_TITLE, entity.getTitle());
        } else {
            values.putNull(COLUMN_TITLE);
        }
        if (!TextUtils.isEmpty(entity.getBody())) {
            values.put(COLUMN_BODY, entity.getBody());
        } else {
            values.putNull(COLUMN_BODY);
        }
        values.put(COLUMN_USER_ID, entity.getUserId());
        return values;
    }
}
