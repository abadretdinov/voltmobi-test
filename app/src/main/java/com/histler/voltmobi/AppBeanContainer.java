package com.histler.voltmobi;

import com.google.gson.Gson;
import com.histler.base.dao.CreateTableDao;
import com.histler.voltmobi.dao.FavPostDao;
import com.histler.voltmobi.dao.PostDao;
import com.histler.voltmobi.remote.JsonPlaceholderRestService;
import com.histler.voltmobi.service.PostService;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Badr
 * on 25.05.2016 14:21
 */
public enum AppBeanContainer {
    INSTANCE;

    private final List<CreateTableDao> mAllDaos = new ArrayList<>();
    private PostDao mPostDao;
    private FavPostDao mFavPostDao;
    private WeakReference<RestAdapter> mRestAdapter;
    private WeakReference<JsonPlaceholderRestService> mJsonPlaceholderRestService;

    private WeakReference<PostService> mPostService;

    AppBeanContainer() {
        mPostDao = new PostDao();
        mFavPostDao = new FavPostDao();
        mAllDaos.add(mPostDao);
        mAllDaos.add(mFavPostDao);
    }

    /*todo move endpoint to properties file, so we can easily change it for debug, if needed*/
    private RestAdapter getRestAdapter() {
        if (mRestAdapter == null || mRestAdapter.get() == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://jsonplaceholder.typicode.com/")
                    .setConverter(new GsonConverter(new Gson()))
                    .build();
            mRestAdapter = new WeakReference<>(restAdapter);
        }
        return mRestAdapter.get();
    }

    public JsonPlaceholderRestService getJsonPlaceHolderRestService() {
        if (mJsonPlaceholderRestService == null || mJsonPlaceholderRestService.get() == null) {
            mJsonPlaceholderRestService = new WeakReference<>(getRestAdapter().create(JsonPlaceholderRestService.class));
        }
        return mJsonPlaceholderRestService.get();
    }

    public PostService getPostService() {
        if (mPostService == null || mPostService.get() == null) {
            mPostService = new WeakReference<>(new PostService());
        }
        return mPostService.get();
    }

    public List<CreateTableDao> getAllDaos() {
        return mAllDaos;
    }

    public PostDao getPostDao() {
        return mPostDao;
    }

    public FavPostDao getFavPostDao() {
        return mFavPostDao;
    }
}