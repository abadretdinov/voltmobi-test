package com.histler.voltmobi.task;

import android.content.Context;

import com.histler.base.util.robospice.TaskRequest;
import com.histler.voltmobi.AppBeanContainer;
import com.histler.voltmobi.entitiy.FavPost;
import com.histler.voltmobi.service.PostService;

/**
 * Created by Badr
 * on 29.05.2016 4:44.
 */
public class FavPostGetRequest extends TaskRequest<FavPost> {
    private Context mContext;
    private long mId;

    public FavPostGetRequest(Context context, long id) {
        super(FavPost.class);
        mContext = context.getApplicationContext();
        mId = id;
    }

    @Override
    public FavPost loadData() throws Exception {
        PostService postService = AppBeanContainer.INSTANCE.getPostService();
        return postService.getFavPost(mContext, mId);
    }
}
