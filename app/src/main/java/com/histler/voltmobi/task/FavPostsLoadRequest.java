package com.histler.voltmobi.task;

import android.content.Context;

import com.histler.voltmobi.AppBeanContainer;
import com.histler.voltmobi.entitiy.FavPost;
import com.histler.voltmobi.entitiy.Post;
import com.histler.voltmobi.remote.JsonPlaceholderRestService;
import com.histler.voltmobi.service.PostService;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by Badr
 * on 29.05.2016 1:28.
 */
public class FavPostsLoadRequest extends SpiceRequest<FavPost.List> {
    private Context mContext;

    public FavPostsLoadRequest(Context context) {
        super(FavPost.List.class);
        mContext = context.getApplicationContext();
    }

    @Override
    public FavPost.List loadDataFromNetwork() throws Exception {
        JsonPlaceholderRestService jsonPlaceholderRestService = AppBeanContainer.INSTANCE.getJsonPlaceHolderRestService();
        Post.List posts = jsonPlaceholderRestService.getPosts();
        PostService postService = AppBeanContainer.INSTANCE.getPostService();
        postService.savePosts(mContext, posts);
        return postService.getFavPosts(mContext);
    }
}
