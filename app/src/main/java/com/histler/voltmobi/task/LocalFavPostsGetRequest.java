package com.histler.voltmobi.task;

import android.content.Context;

import com.histler.base.util.robospice.TaskRequest;
import com.histler.voltmobi.AppBeanContainer;
import com.histler.voltmobi.entitiy.FavPost;
import com.histler.voltmobi.service.PostService;

/**
 * Created by Badr
 * on 29.05.2016 2:48.
 */
public class LocalFavPostsGetRequest extends TaskRequest<FavPost.List> {
    private Context mContext;

    public LocalFavPostsGetRequest(Context context) {
        super(FavPost.List.class);
        mContext = context.getApplicationContext();
    }

    @Override
    public FavPost.List loadData() throws Exception {
        PostService postService = AppBeanContainer.INSTANCE.getPostService();
        return postService.getFavPosts(mContext);
    }
}
