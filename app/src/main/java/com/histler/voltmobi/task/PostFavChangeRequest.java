package com.histler.voltmobi.task;

import android.content.Context;

import com.histler.base.util.robospice.TaskRequest;
import com.histler.voltmobi.AppBeanContainer;
import com.histler.voltmobi.entitiy.FavPost;
import com.histler.voltmobi.service.PostService;

/**
 * Created by Badr
 * on 29.05.2016 3:12.
 */
public class PostFavChangeRequest extends TaskRequest<FavPost> {
    private Context mContext;
    private FavPost mFavPost;

    public PostFavChangeRequest(Context context, FavPost favPost) {
        super(FavPost.class);
        mContext = context.getApplicationContext();
        mFavPost = favPost;
    }

    @Override
    public FavPost loadData() throws Exception {
        PostService postService = AppBeanContainer.INSTANCE.getPostService();
        postService.setFav(mContext, mFavPost);
        return postService.getFavPost(mContext, mFavPost.getId());
    }
}
