package com.histler.voltmobi.entitiy;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Badr
 * on 28.05.2016.
 */
public class FavPost extends Post {
    private boolean isFav;

    public FavPost() {
        super();
    }

    public FavPost(Post post, boolean isFav) {
        this(post.getId(), post.getUserId(), post.getTitle(), post.getBody(), isFav);
    }

    public FavPost(long id, long userId, String title, String body, boolean isFav) {
        super(id, userId, title, body);
        this.isFav = isFav;
    }

    public boolean isFav() {
        return isFav;
    }

    public void setFav(boolean fav) {
        isFav = fav;
    }

    public static class List extends ArrayList<FavPost> {
        public List(int capacity) {
            super(capacity);
        }

        public List() {
        }

        public List(Collection<? extends FavPost> collection) {
            super(collection);
        }
    }
}
