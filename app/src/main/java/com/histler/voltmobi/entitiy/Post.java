package com.histler.voltmobi.entitiy;

import com.histler.base.entity.IHasId;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Badr
 * on 25.05.2016 11:40
 */
public class Post implements Serializable, IHasId {
    private long id;
    private long userId;
    private String title;
    private String body;

    public Post() {
    }

    public Post(long id, long userId, String title, String body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Post post = (Post) o;

        return id == post.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    public static class List extends ArrayList<Post> {
        public List(int capacity) {
            super(capacity);
        }

        public List() {
        }

        public List(Collection<? extends Post> collection) {
            super(collection);
        }
    }
}
