package com.histler.voltmobi;

import com.histler.base.BaseBeanContainer;
import com.histler.voltmobi.service.NavigationServiceImpl;

/**
 * Created by Badr
 * on 28.05.2016 22:59.
 */
public final class Initializer {
    protected static void initialize() {
        BaseBeanContainer baseBeanContainer = BaseBeanContainer.INSTANCE;
        baseBeanContainer.setNavigationService(new NavigationServiceImpl());

        AppBeanContainer appBeanContainer = AppBeanContainer.INSTANCE;
        baseBeanContainer.getAllDaos().addAll(appBeanContainer.getAllDaos());
    }
}
