package com.histler.base.adapter;

import android.support.v7.widget.RecyclerView;

import com.histler.base.adapter.viewholder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Badr
 * on 26.05.2016 17:50
 */
public abstract class BaseRecyclerAdapter<T, R extends BaseViewHolder> extends RecyclerView.Adapter<R> {
    protected List<T> mData;
    protected OnItemClickListener mOnItemClickListener;
    protected OnItemLongClickListener mOnItemLongClickListener;

    public BaseRecyclerAdapter(List<T> data) {
        mData = data != null ? data : new ArrayList<T>();
    }

    public void setData(List<T> data) {
        mData = data != null ? data : new ArrayList<T>();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void removeItem(T item) {
        int position = mData.indexOf(item);
        if (position >= 0) {
            removeItem(position);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onClickListener) {
        mOnItemClickListener = onClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onLongClickListener) {
        mOnItemLongClickListener = onLongClickListener;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public T getItem(int position) {
        return mData.get(position);
    }

    public List<T> getItems() {
        return mData;
    }
}
