package com.histler.base.adapter;

import android.view.View;

/**
 * Created by Badr
 * 19.02.2015
 * 12:41
 */
public interface OnItemLongClickListener {
    boolean onItemLongClick(View view, int position);
}
