package com.histler.base.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.histler.base.adapter.OnItemClickListener;
import com.histler.base.adapter.OnItemLongClickListener;

/**
 * Created by Badr
 * on 26.05.2016 17:50
 */
public abstract class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
    private OnItemClickListener mOnClickListener;
    private OnItemLongClickListener mOnLongClickListener;

    public BaseViewHolder(View itemView) {
        this(itemView, null);
    }

    public BaseViewHolder(View itemView, OnItemClickListener clickListener) {
        this(itemView, clickListener, null);
    }

    public BaseViewHolder(View itemView, OnItemClickListener clickListener, OnItemLongClickListener longClickListener) {
        super(itemView);
        mOnClickListener = clickListener;
        mOnLongClickListener = longClickListener;
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
        initView(itemView);
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        mOnClickListener = clickListener;
    }

    public void setOnItemLongCLickListener(OnItemLongClickListener longClickListener) {
        mOnLongClickListener = longClickListener;
    }

    @Override
    public void onClick(View view) {
        if (view.equals(itemView)) {
            if (mOnClickListener != null) {
                mOnClickListener.onItemClick(view, getLayoutPosition());
            }
        }
    }

    protected abstract void initView(View itemView);

    @Override
    public boolean onLongClick(View view) {
        if (view.equals(itemView)) {
            return mOnLongClickListener != null && mOnLongClickListener.onItemLongClick(view, getLayoutPosition());
        }
        return false;
    }
}
