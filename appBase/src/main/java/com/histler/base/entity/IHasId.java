package com.histler.base.entity;

/**
 * Created by Badr
 * on 28.05.2016 23:57.
 */
public interface IHasId {
    long getId();
}
