package com.histler.base.entity;

/**
 * Created by Badr on 28.05.2016.
 */
public class LineItem {
    public int sectionFirstPosition;
    public boolean isHeader;
    public boolean isEnabled;
    public Object data;

    public LineItem(int sectionFirstPosition, boolean isHeader, boolean isEnabled, Object data) {
        this.sectionFirstPosition = sectionFirstPosition;
        this.isHeader = isHeader;
        this.isEnabled = isEnabled;
        this.data = data;
    }

    public LineItem(int sectionFirstPosition, boolean isHeader, Object data) {
        this(sectionFirstPosition, isHeader, true, data);
    }
}
