package com.histler.base.util;

/**
 * Created by Badr
 * on 28.05.2016.
 */
public interface BackListener {
    boolean onBackPressed();
}
